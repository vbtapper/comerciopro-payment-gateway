FROM java:8
MAINTAINER evanilsonbraulio@gmail.com

VOLUME /tmp
EXPOSE 9000
 
ENV USER_NAME comerciopro
ENV APP_HOME /home/$USER_NAME/app

RUN useradd -ms /bin/bash $USER_NAME
RUN mkdir $APP_HOME

ADD target/payment-gateway-0.0.1-SNAPSHOT.jar $APP_HOME/payment-gateway-0.0.1-SNAPSHOT.jar
ADD jars/ekumbu-java-1.0-SNAPSHOT-jar-with-dependencies.jar $APP_HOME/ekumbu-java-1.0-SNAPSHOT-jar-with-dependencies.jar

RUN chown $USER_NAME $APP_HOME/payment-gateway-0.0.1-SNAPSHOT.jar
CMD ["mvn install:install-file -Dfile=ekumbu-java-1.0-SNAPSHOT-jar-with-dependencies.jar -DgroupId=com.nossapps.ekumbu -DartifactId=ekumbu-java -Dversion=0.0.1 -Dpackaging=jar"]

USER $USER_NAME
WORKDIR $APP_HOME
RUN bash -c 'touch payment-gateway-0.0.1-SNAPSHOT.jar'

ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","payment-gateway-0.0.1-SNAPSHOT.jar"]