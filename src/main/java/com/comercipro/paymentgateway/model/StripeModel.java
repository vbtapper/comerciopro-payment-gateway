package com.comercipro.paymentgateway.model;


public class StripeModel {

	private String cardNumber;
	private String expMonth;
	private String expYear;
	private String ccv;
	private String description;
	private Integer amount;
	private String customerIP;
	private String accessKey;
	private String TeamID;
	private String currency;
	private Double originalPaymentValue;
	
	public StripeModel() {
		
	}
	
	public String getCardNumber() {
		return cardNumber;
	}
	
	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}
	
	public String getExpMonth() {
		return expMonth;
	}
	
	public void setExpMonth(String expMonth) {
		this.expMonth = expMonth;
	}
	
	public String getExpYear() {
		return expYear;
	}
	
	public void setExpYear(String expYear) {
		this.expYear = expYear;
	}
	
	public String getCcv() {
		return ccv;
	}
	
	public void setCcv(String ccv) {
		this.ccv = ccv;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public Integer getAmount() {
		return amount;
	}
	
	public void setAmount(Integer amount) {
		this.amount = amount;
	}
	
	public String getCustomerIP() {
		return customerIP;
	}
	
	public void setCustomerIP(String customerIP) {
		this.customerIP = customerIP;
	}
	
	public String getAccessKey() {
		return accessKey;
	}
	
	public void setAccessKey(String accessKey) {
		this.accessKey = accessKey;
	}
	
	public String getTeamID() {
		return TeamID;
	}
	
	public void setTeamID(String teamID) {
		TeamID = teamID;
	}
	
	public String getCurrency() {
		return currency;
	}
	
	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public Double getOriginalPaymentValue() {
		return originalPaymentValue;
	}

	public void setOriginalPaymentValue(Double originalPaymentValue) {
		this.originalPaymentValue = originalPaymentValue;
	}
	
}
