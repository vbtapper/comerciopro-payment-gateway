package com.comercipro.paymentgateway.model;

import java.util.Date;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document
public class Payments {

	@Id
	private ObjectId id;
	
	@Field("created")
	private Date created;
	
	@Field("teamID")
	private String teamID;
	
	@Field("processorType")
	private String processorType;
	
	@Field("processorPaymentID")
	private String processorPaymentID;
	
	@Field("amount")
	private Double amount;
	
	@Field("purchaseID")
	private String purchaseID;
	
	@Field("status")
	private String status;
	
	@Field("currency")
	private String currency;
	
	public Payments() {}
	
	@PersistenceConstructor
	public Payments(Date created, String teamID, String processorType, String processorPaymentID, Double amount, String status, String currency) {
	    this.created = created;
	    this.teamID = teamID;
	    this.processorType = processorType;
	    this.processorPaymentID = processorPaymentID;
	    this.amount = amount;
	    this.status = status;
	    this.currency = currency;
	}
	
	public ObjectId getId() {
		return id;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public String getTeamID() {
		return teamID;
	}

	public void setTeamID(String teamID) {
		this.teamID = teamID;
	}

	public String getProcessorType() {
		return processorType;
	}

	public void setProcessorType(String processorType) {
		this.processorType = processorType;
	}

	public String getProcessorPaymentID() {
		return processorPaymentID;
	}

	public void setProcessorPaymentID(String processorPaymentID) {
		this.processorPaymentID = processorPaymentID;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getPurchaseID() {
		return purchaseID;
	}

	public void setPurchaseID(String purchaseID) {
		this.purchaseID = purchaseID;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}
	
}
