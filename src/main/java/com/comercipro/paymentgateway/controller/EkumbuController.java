package com.comercipro.paymentgateway.controller;

import java.util.Map;
import java.util.Date;
import java.util.HashMap;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.comercipro.paymentgateway.configuration.PaymentRepository;
import com.comercipro.paymentgateway.model.EkumbuModel;
import com.comercipro.paymentgateway.model.Payments;
import com.nossapps.ekumbu.exception.APIException;
import com.nossapps.ekumbu.exception.AuthenticationException;
import com.nossapps.ekumbu.model.Payment;
import com.nossapps.ekumbu.model.Tokens;
import com.nossapps.ekumbu.net.RequestOptions;


@RestController
@RequestMapping("/ekumbu")
public class EkumbuController {

	static String LIVEMODE = "false";
	
	@Autowired
	PaymentRepository paymentRepository;
	
	@RequestMapping(value="/health", method = RequestMethod.GET)
	public @ResponseBody Map<String, Object> welcome() {
		
		HashMap<String, Object> map = new HashMap<>();
	    map.put("error", null);
	    map.put("statusCode", 200);
	    map.put("message", "success");
	    
	    return map;
		
	}
	
	@RequestMapping(value="payment", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> payment(@RequestBody EkumbuModel ekbModel) {
		
		HashMap<String, Object> paramP = new HashMap<String, Object>();
		HashMap<String, Object> paramT = new HashMap<String, Object>();
		HashMap<String, Object> mapRsp = new HashMap<>();
		
		Tokens tokens = new Tokens();
		Payment payment = new Payment();
		
		paramT.put("cardnumber", ekbModel.getCardNumber());
		paramT.put("expmonth", ekbModel.getExpMonth());
		paramT.put("expyear", ekbModel.getExpYear());
		paramT.put("customerip", ekbModel.getCustomerIP());
		paramT.put("livemode", LIVEMODE);
		
		RequestOptions options = RequestOptions.builder().setApiKey(ekbModel.getAccessKey()).build();
		
		try {
			JSONObject TknRps = tokens.create(paramT, options);
				
			paramP.put("source", TknRps.get("tokenid"));
			paramP.put("amount", ekbModel.getAmount());
			paramP.put("description", ekbModel.getDescription());
			paramP.put("livemode", LIVEMODE);
			
			JSONObject payresp = payment.Charge(paramP, options);
			
			if(payresp.getString("message").compareTo("Authorization Required") != 0) {
				
				Payments pymModel = new Payments();
				
				pymModel.setAmount(ekbModel.getAmount());
				pymModel.setCreated(new Date());
				pymModel.setCurrency(ekbModel.getCurrency());
				pymModel.setProcessorPaymentID(payresp.getString("paymentid"));
				pymModel.setProcessorType("ekumbu");
				pymModel.setStatus("paid");
				pymModel.setTeamID(ekbModel.getTeamID());
				
				paymentRepository.save(pymModel);
				mapRsp.put("paymentTokenID", pymModel.getId().toString());
				mapRsp.put("dateProcessed", pymModel.getCreated().toString());
			}
			
			mapRsp.put("statusCode", payresp.getInt("statusCode"));
			mapRsp.put("message", payresp.getString("message"));
			mapRsp.put("paymentid", payresp.getString("paymentid"));

			return mapRsp;
			
		} catch (APIException e) {	
			
			e.printStackTrace();
			
			mapRsp.put("error", "Request Error");
			mapRsp.put("statusCode", 403);
			mapRsp.put("message", e.getMessage());
		    
		    return mapRsp;
		}
		
		catch (AuthenticationException e) {
			e.printStackTrace();
			
			mapRsp.put("error", "Forbiden");
			mapRsp.put("statusCode", 403);
			mapRsp.put("message", e.getMessage());
		    
		    return mapRsp;
		}
	}
	
	@RequestMapping(value="authorization", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> authorization(@RequestBody EkumbuModel ekbModel) {
		HashMap<String, Object> mapRsp = new HashMap<>();
		
		return mapRsp;
	}
}
