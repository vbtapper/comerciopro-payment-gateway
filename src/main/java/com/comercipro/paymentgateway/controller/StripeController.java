package com.comercipro.paymentgateway.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.comercipro.paymentgateway.configuration.PaymentRepository;
import com.comercipro.paymentgateway.model.Payments;
import com.comercipro.paymentgateway.model.StripeModel;
import com.stripe.exception.StripeException;
import com.stripe.model.Charge;
import com.stripe.model.Token;
import com.stripe.net.RequestOptions;

@RestController
@RequestMapping("/stripe")
public class StripeController {

	@Autowired
	PaymentRepository paymentRepository;
	
	@RequestMapping(value="/health", method = RequestMethod.GET)
	public @ResponseBody Map<String, Object> welcome() {
		
		HashMap<String, Object> map = new HashMap<>();
	    map.put("error", null);
	    map.put("statusCode", 200);
	    map.put("message", "success");
	    
	    return map;
		
	}
	
	@RequestMapping(value="payment", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> payment(@RequestBody StripeModel stripeModel) {
		
		RequestOptions requestOptions = RequestOptions.builder().setApiKey(stripeModel.getAccessKey()).build();
		HashMap<String, Object> mapRsp = new HashMap<>();
		
        try {
        	
	        	Map<String, Object> tokenParams = new HashMap<String, Object>();
	        	Map<String, Object> cardParams = new HashMap<String, Object>();
	        	cardParams.put("number", stripeModel.getCardNumber());
	        	cardParams.put("exp_month", stripeModel.getExpMonth());
	        	cardParams.put("exp_year", stripeModel.getExpYear());
	        	cardParams.put("cvc", stripeModel.getCcv());
	        	tokenParams.put("card", cardParams);

	        Token newToken =	Token.create(tokenParams, requestOptions);
	        	
        		Map<String, Object> chargeMap = new HashMap<String, Object>();
            chargeMap.put("amount", stripeModel.getAmount());
            chargeMap.put("currency", stripeModel.getCurrency());
            chargeMap.put("source", newToken.getId());
            
            Charge charge = Charge.create(chargeMap, requestOptions);
            
            Payments pymModel = new Payments();
            
            pymModel.setAmount(stripeModel.getOriginalPaymentValue());
			pymModel.setCreated(new Date());
			pymModel.setCurrency(stripeModel.getCurrency());
			pymModel.setProcessorPaymentID(charge.getId());
			pymModel.setProcessorType("stripe");
			pymModel.setStatus("paid");
			pymModel.setTeamID(stripeModel.getTeamID());
			
			paymentRepository.save(pymModel);
			
			mapRsp.put("statusCode", 200);
			mapRsp.put("message", "success");
			mapRsp.put("paymentid", charge.getId());
			mapRsp.put("paymentTokenID", pymModel.getId().toString());
			mapRsp.put("dateProcessed", pymModel.getCreated().toString());
			
			return mapRsp;
			
        } catch (StripeException e) {
            e.printStackTrace();
            
            mapRsp.put("error", "Request Error");
			mapRsp.put("statusCode", e.getStatusCode());
			mapRsp.put("message", e.getMessage());
		    
		    return mapRsp;
        }
	}
}
