package com.comercipro.paymentgateway.configuration;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.comercipro.paymentgateway.model.Payments;

public interface PaymentRepository extends MongoRepository<Payments, String> {
}
