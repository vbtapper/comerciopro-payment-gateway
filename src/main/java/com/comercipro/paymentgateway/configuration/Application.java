package com.comercipro.paymentgateway.configuration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import com.comercipro.paymentgateway.controller.EkumbuController;

@SpringBootApplication
@ComponentScan(basePackageClasses = EkumbuController.class)
public class Application {
	
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
    }

}